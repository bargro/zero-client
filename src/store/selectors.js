export const getGameStage = state => {
  return state.gameState.stage;
};

export const getPlayerName = state => {
  return state.gameState.player.name;
};
export const getOtherPlayers = state => {
  return state.gameState.otherPlayers;
};

export const getCardsOnTable = state => {
  return state.gameState.cardsOnTable;
};
export const getPlayerCards = state => {
  return state.gameState.player.cards;
};
export const getCurrentPlayerUuid = state => {
  return state.gameState.currentPlayerId;
};

export const canPlayerMove = state => {
  return state.gameState.currentPlayerId === state.playerUuid;
};

export const getPlayerSelectedCard = state => {
  return state.selectedCards.player;
};

export const getTableSelectedCard = state => {
  return state.selectedCards.table;
};

export const areBothCardSelected = state => {
  const selectedCards = state.selectedCards;
  return selectedCards.table !== -1 && selectedCards.player !== -1;
};

export const getGameResult = state => {
  const currentPlayer = state.gameState.player;
  const otherPlayers = state.gameState.otherPlayers;

  const results = [];
  results.push({ name: currentPlayer.name, points: currentPlayer.points });
  otherPlayers.forEach(element => {
    results.push({ name: element.name, points: element.points });
  });
  results.sort(compareForResults);
  return results;
};

const compareForResults = (playerA, playerB) => {
  if (playerA.points < playerB.points) {
    return -1;
  }
  if (playerA.points > playerB.points) {
    return 1;
  }
  return 0;
};
