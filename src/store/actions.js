export const GAME_STATE_RECEIVED = "GAME_STATE_RECEIVED";
export const PLAYER_UUID_RECEIVED = "PLAYER_UUID_RECEIVED";
export const CARD_ON_TABLE_SELECTED = "CARD_ON_TABLE_SELECTED";
export const PLAYER_CARD_SELECTED = "PLAYER_CARD_SELECTED";
export const CLEAR_SELECTION = "CLEAR_SELECTION";

export const playerUuidReceived = playerUuid => {
  return { type: PLAYER_UUID_RECEIVED, payload: playerUuid };
};

export const gameStateReceived = gameState => {
  return { type: GAME_STATE_RECEIVED, payload: gameState };
};

export const tableCardSelected = cardId => {
  return { type: CARD_ON_TABLE_SELECTED, payload: cardId };
};

export const playerCardSelected = cardId => {
  return { type: PLAYER_CARD_SELECTED, payload: cardId };
};

export const clearSelectedCards = () => {
  return { type: CLEAR_SELECTION };
};
