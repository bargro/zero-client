import { combineReducers } from "redux";
import GAME_STAGE from "../entity/GameStage";
import {
  CARD_ON_TABLE_SELECTED,
  CLEAR_SELECTION,
  GAME_STATE_RECEIVED,
  PLAYER_CARD_SELECTED,
  PLAYER_UUID_RECEIVED
} from "./actions";
const initialGameState = {
  stage: GAME_STAGE.START
};

const gameStateReducer = (state = initialGameState, action) => {
  if (GAME_STATE_RECEIVED === action.type) {
    return action.payload;
  }
  return state;
};

const playerUuidReducer = (state = null, action) => {
  if (PLAYER_UUID_RECEIVED === action.type) {
    return action.payload;
  }
  return state;
};

const selectedCardInit = {
  table: -1,
  player: -1
};

const cardSelectionReducer = (state = selectedCardInit, action) => {
  switch (action.type) {
    case CARD_ON_TABLE_SELECTED:
      return { ...state, table: action.payload };
    case PLAYER_CARD_SELECTED:
      return { ...state, player: action.payload };
    case CLEAR_SELECTION:
      return selectedCardInit;

    default:
      return state;
  }
};

const rootReducer = combineReducers({
  gameState: gameStateReducer,
  playerUuid: playerUuidReducer,
  selectedCards: cardSelectionReducer
});

export default rootReducer;
