import gameApi from "../api/GameApi";
import { startGamePolling } from "../game/GameStatePolling";
import { playerUuidReceived } from "../store/actions";
import store from "../store/store";

export const joinGame = async name => {
  const playerData = await gameApi.joinGameRequest(name);
  if (playerData) {
    store.dispatch(playerUuidReceived(playerData.uuid));
  } else {
    const someUuid = "6845e0bb-2359-491b-bd10-651c5525859b";
    store.dispatch(playerUuidReceived(someUuid));
  }
  startGamePolling();
};
