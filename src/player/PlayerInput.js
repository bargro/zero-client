import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import Grid from "@material-ui/core/Grid";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import React, { Component } from "react";
import { joinGame } from "./JoinGame";
export default class PlayerInput extends Component {
  state = {
    inputValue: ""
  };

  handleChange = event => {
    this.setState({ inputValue: event.target.value });
  };

  render() {
    return (
      <div style={{ textAlign: "center" }}>
        <Grid container direction="column" justify="center" alignItems="center">
          <FormControl>
            <InputLabel htmlFor="component-simple">Gracz</InputLabel>
            <Input
              id="component-simple"
              value={this.state.inputValue}
              onChange={this.handleChange}
            />
          </FormControl>
          <Button
            style={{ marginTop: "20px" }}
            onClick={() => joinGame(this.state.inputValue)}
            variant="contained"
            disabled={this.state.inputValue === ""}
            color="primary"
          >
            DOŁĄCZ DO GRY
          </Button>
        </Grid>
      </div>
    );
  }
}
