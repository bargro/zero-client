import React, { Component } from "react";
import { connect } from "react-redux";
import { getOtherPlayers, getPlayerName } from "../store/selectors";
class WaitingForPlayers extends Component {
  render() {
    const { playerName, otherPlayers } = this.props;
    return (
      <div>
        <h1>WITAJ {playerName}</h1>
        <p>ZARAZ ROZPOCZNIE SIĘ GRA</p>
        <p>OCZEKIWANIE NA POZOSTAŁYCH GRACZY</p>
        <p>DO ROZPOCZĘCIA GRY WYMAGANE JEST TROJE GRACZY</p>
        <p>DO TEJ PORY DO GRY DOŁĄCZYLI:</p>
        {this.renderOthers(otherPlayers)}
      </div>
    );
  }
  renderOthers = otherPlayers => {
    const playersComp = otherPlayers.map((player, index) => {
      return <p key={index}>{player.name}</p>;
    });
    return <div>{playersComp}</div>;
  };
}

const mapStateToProps = state => {
  return {
    playerName: getPlayerName(state),
    otherPlayers: getOtherPlayers(state)
  };
};

export default connect(mapStateToProps)(WaitingForPlayers);
