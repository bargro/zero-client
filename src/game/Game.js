import React, { Component } from "react";
import { connect } from "react-redux";
import GAME_STAGE from "../entity/GameStage";
import GamePlay from "../gameplay/GamePlay";
import GameResults from "../gameresults/GameResults";
import PlayerInput from "../player/PlayerInput";
import { getGameStage } from "../store/selectors";
import WaitingForPlayers from "../waitingForPlayers/WaitingForPlayers";

class Game extends Component {
  render() {
    return <div>{this.renderForGameStage(this.props.gameStage)}</div>;
  }

  renderForGameStage = gameStage => {
    switch (gameStage) {
      case GAME_STAGE.START:
        return <PlayerInput />;
      case GAME_STAGE.WAITING_FOR_PLAYERS:
        return <WaitingForPlayers />;
      case GAME_STAGE.GAME:
        return <GamePlay />;
      case GAME_STAGE.END:
        return <GameResults />;
      default:
        return <p>DEFAULT</p>;
    }
  };
}

const mapStateToProps = state => {
  return {
    gameStage: getGameStage(state)
  };
};

export default connect(mapStateToProps)(Game);
