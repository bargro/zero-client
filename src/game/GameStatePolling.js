import gameApi from "../api/GameApi";
import { gameStateReceived } from "../store/actions";
import store from "../store/store";

export const startGamePolling = () => {
  setInterval(async () => {
    const playerUuid = store.getState().playerUuid;
    const gameState = await gameApi.getGameStateForPlayer(playerUuid);
    if (gameState) {
      store.dispatch(gameStateReceived(gameState));
    } else {
      store.dispatch(gameStateReceived({ stage: "DEFAULT" }));
    }
  }, 1000);
};
