import React, { Component } from "react";

export default class OtherPlayers extends Component {
  render() {
    return (
      <div>
        <h2>PRZECIWNICY:</h2>
        {this.renderPlayers()}
      </div>
    );
  }
  renderPlayers = () => {
    const { players, currentPlayerUuid } = this.props;
    const playersItems = players.map((player, index) => {
      const hasMove = player.uuid === currentPlayerUuid;
      return (
        <div key={index}>
          <p>
            {`${hasMove ? "WYKONUJE RUCH: " : ""} ${
              player.name
            } - Liczba PASów: ${player.pasCounter}`}
          </p>
        </div>
      );
    });
    return playersItems;
  };
}
