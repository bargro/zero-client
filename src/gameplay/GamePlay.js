import Button from "@material-ui/core/Button";
import React, { Component } from "react";
import { connect } from "react-redux";
import { makeMove, makeNoBid } from "../move/Move";
import { clearSelectedCards } from "../store/actions";
import {
  areBothCardSelected,
  canPlayerMove,
  getCurrentPlayerUuid,
  getOtherPlayers,
  getPlayerName
} from "../store/selectors";
import OtherPlayers from "./OtherPlayers";
import PlayerCards from "./PlayerCards";
import TableCards from "./TableCards";
class GamePlay extends Component {
  render() {
    const {
      playerName,
      otherPlayers,
      currentPlayerUuid,
      areBothCardSelected,
      canPlayerMove
    } = this.props;
    return (
      <div>
        <h1>ZERO GAME!!!</h1>
        <OtherPlayers
          players={otherPlayers}
          currentPlayerUuid={currentPlayerUuid}
        />
        <TableCards />
        <h2>Twoje karty, {playerName}:</h2>
        <PlayerCards />
        <Button
          style={{ marginTop: "20px" }}
          onClick={() => this.onMakeMoveClick()}
          variant="contained"
          disabled={!areBothCardSelected || !canPlayerMove}
          color="primary"
        >
          WYKONAJ RUCH
        </Button>

        <Button
          style={{ marginTop: "20px", marginLeft: "10px" }}
          onClick={() => this.onPasClick()}
          variant="contained"
          disabled={!canPlayerMove}
          color="primary"
        >
          PAS
        </Button>
      </div>
    );
  }
  onMakeMoveClick = () => {
    const { clearSelectedCards } = this.props;
    makeMove();
    clearSelectedCards();
  };
  onPasClick = () => {
    const { clearSelectedCards } = this.props;
    makeNoBid();
    clearSelectedCards();
  };
}

const mapDispatchToProps = dispatch => ({
  clearSelectedCards: () => dispatch(clearSelectedCards())
});

const mapStateToProps = state => {
  return {
    playerName: getPlayerName(state),
    otherPlayers: getOtherPlayers(state),
    currentPlayerUuid: getCurrentPlayerUuid(state),
    areBothCardSelected: areBothCardSelected(state),
    canPlayerMove: canPlayerMove(state)
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GamePlay);
