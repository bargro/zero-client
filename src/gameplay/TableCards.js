import Grid from "@material-ui/core/Grid";
import React, { Component } from "react";
import { connect } from "react-redux";
import { tableCardSelected } from "../store/actions";
import {
  canPlayerMove,
  getCardsOnTable,
  getTableSelectedCard
} from "../store/selectors";
import Card from "./Card";
class TableCards extends Component {
  render() {
    return (
      <div>
        <h2>KARTY NA STOLE</h2>
        {this.renderCards()}
      </div>
    );
  }
  renderCards = () => {
    const {
      cardsOnTable,
      selectedCard,
      tableCardSelected,
      canPlayerMove
    } = this.props;
    const cardComponents = Object.keys(cardsOnTable).map(cardKey => {
      return (
        <Card
          key={cardKey}
          card={cardsOnTable[cardKey]}
          selected={selectedCard === cardKey}
          onClick={() => {
            if (canPlayerMove) {
              tableCardSelected(cardKey);
            }
          }}
        />
      );
    });

    return (
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
        spacing={16}
      >
        {cardComponents}
      </Grid>
    );
  };
}

const mapDispatchToProps = dispatch => ({
  tableCardSelected: cardId => dispatch(tableCardSelected(cardId))
});

const mapStateToProps = state => {
  return {
    cardsOnTable: getCardsOnTable(state),
    selectedCard: getTableSelectedCard(state),
    canPlayerMove: canPlayerMove(state)
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TableCards);
