import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import React, { Component } from "react";
export default class Card extends Component {
  render() {
    const { card, selected, onClick } = this.props;
    return (
      <Paper
        style={{
          height: 140,
          width: 100,
          background: this.getStyleColor(card.color),
          borderStyle: selected ? "solid" : ""
        }}
        onClick={() => {
          onClick();
        }}
      >
        <Typography variant="h5" component="h2">
          {card.value}
        </Typography>
        <Typography variant="h5" component="h1" style={{ fontWeight: "bold" }}>
          {this.getNumberValue(card.value)}
        </Typography>
      </Paper>
    );
  }

  getStyleColor = color => {
    switch (color) {
      case "GREEN":
        return "green";
      case "RED":
        return "red";
      case "PINK":
        return "pink";
      case "BLUE":
        return "blue";
      case "GREY":
        return "grey";
      case "BROWN":
        return "brown";
      case "YELLOW":
        return "yellow";

      default:
        return "white";
    }
  };

  getNumberValue = value => {
    switch (value) {
      case "ONE":
        return 1;
      case "TWO":
        return 2;
      case "THREE":
        return 3;
      case "FOUR":
        return 4;
      case "FIVE":
        return 5;
      case "SIX":
        return 6;
      case "SEVEN":
        return 7;
      case "EIGHT":
        return 8;
      default:
        return 0;
    }
  };
}
