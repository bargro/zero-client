import Grid from "@material-ui/core/Grid";
import React, { Component } from "react";
import { connect } from "react-redux";
import { playerCardSelected } from "../store/actions";
import {
  canPlayerMove,
  getPlayerCards,
  getPlayerSelectedCard
} from "../store/selectors";
import Card from "./Card";
class PlayerCards extends Component {
  render() {
    return (
      <div>
        <h2>KARTY GRACZA</h2>
        {this.renderCards()}
      </div>
    );
  }

  renderCards = () => {
    const {
      playerCards,
      selectedCard,
      playerCardSelected,
      canPlayerMove
    } = this.props;
    const cardComponents = Object.keys(playerCards).map(cardKey => {
      return (
        <Card
          key={cardKey}
          card={playerCards[cardKey]}
          selected={selectedCard === cardKey}
          onClick={() => {
            if (canPlayerMove) {
              playerCardSelected(cardKey);
            }
          }}
        />
      );
    });

    return (
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
        spacing={16}
      >
        {cardComponents}
      </Grid>
    );
  };
}

const mapDispatchToProps = dispatch => ({
  playerCardSelected: cardId => dispatch(playerCardSelected(cardId))
});

const mapStateToProps = state => {
  return {
    playerCards: getPlayerCards(state),
    selectedCard: getPlayerSelectedCard(state),
    canPlayerMove: canPlayerMove(state)
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PlayerCards);
