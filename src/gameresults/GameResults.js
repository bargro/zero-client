import React, { Component } from "react";
import { connect } from "react-redux";
import { getGameResult } from "../store/selectors";

class GameResults extends Component {
  render() {
    const { gameResult } = this.props;
    return (
      <div>
        <h1>KONIEC GRY</h1>
        <h2>ZWYCIĘZCA: {gameResult[0].name}</h2>
        <h3>Liczba punktów:</h3>
        {this.renderResult(gameResult)}
      </div>
    );
  }
  renderResult = gameResult => {
    return gameResult.map((element, index) => {
      return <h3 key={index}>{`${element.name}: ${element.points}`}</h3>;
    });
  };
}

const mapStateToProps = state => {
  return {
    gameResult: getGameResult(state)
  };
};

export default connect(mapStateToProps)(GameResults);
