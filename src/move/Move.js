import api from "../api/GameApi";
import store from "../store/store";
export const makeMove = async () => {
  const { selectedCards, playerUuid } = store.getState();
  const requestData = {
    chosenCardKey: selectedCards.table,
    rejectedCardKey: selectedCards.player
  };
  api.makeMoveRequest(playerUuid, requestData);
};

export const makeNoBid = async () => {
  const { playerUuid } = store.getState();
  api.makeNoBidRequest(playerUuid);
};
