import axios from "axios";
class GameApi {
  _api = null;
  constructor() {
    this._createApi();
  }
  _createApi = () => {
    this._api = axios.create({
      baseURL: "http://localhost:8080/zero-server/resources",
      timeout: 10000,
      withCredentials: true,
      // transformRequest: [(data) => JSON.stringify(data.data)],
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    });
  };

  joinGameRequest = async name => {
    try {
      const response = await this._api.post("/players?name=" + name);
      console.debug("JOIN GAME: ", response);
      return response.data;
    } catch (err) {
      console.info("JOIN GAME ERROR:", err);
    }
  };
  getGameStateForPlayer = async playerUuid => {
    try {
      const response = await this._api.get("/game/" + playerUuid);
      console.debug("GAME STATE: ", response);
      return response.data;
    } catch (err) {
      console.info("GAME STATE ERROR:", err);
    }
  };

  makeMoveRequest = async (playerUuid, moveData) => {
    try {
      const response = await this._api.post("/move/" + playerUuid, moveData);
      console.debug("MOVE: ", response);
    } catch (err) {
      console.info("MOVE ERROR:", err);
    }
  };
  makeNoBidRequest = async playerUuid => {
    try {
      const response = await this._api.post("/move/" + playerUuid + "/pas");
      console.debug("MOVE NO BID: ", response);
    } catch (err) {
      console.info("MOVE NO BID ERROR:", err);
    }
  };
}
const apiInstance = new GameApi();
export default apiInstance;
