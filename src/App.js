import React from "react";
import "./App.css";
import Game from "./game/Game";
export default class App extends React.Component {
  render() {
    return (
      <div className="App">
        <Game />
      </div>
    );
  }
}
